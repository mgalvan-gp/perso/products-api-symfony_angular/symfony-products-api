# Symfony products API
(Projet en cours)

API de divers produits.

Le but :
* Créer une API sous Symfony 6
* Consolider les compétences acquises
* Consommer l'API avec un front du type **Angular**

Technologies utilisées :
* PHP 
    * Symfony - Doctrine 

Que fait l'API ? :
* Possibilité de **GET** un ou tous les produits

A venir :
* Ajouter/Modier des produits
* Supprimer des produits
* Création d'une entité **User** pour travailler les relations de clés étrangères
* Tests unitaires et fonctionnels (PHPUnit)
