<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Context\Normalizer\ObjectNormalizerContextBuilder;

#[Route('/api')]
class ProductController extends AbstractController
{

    #[Route('/products', name: 'products', methods: 'GET')]
    public function allProducts(ProductRepository $productRepo): JsonResponse
    {
        $products = $productRepo->findAll();

        $data = [];

        foreach ($products as $product) {
            $data[] = [
                'id' => $product->getId(),
                'title' => $product->getTitle(),
                'price' => $product->getPrice(),
                'image' => $product->getImage()
            ];
        }

        return $this->json($data);
    }

    #[Route('/product/{id}', name: 'product', methods: 'GET')]
    public function oneProduct(int $id, ProductRepository $productRepo)
    {
        $product = $productRepo->find($id);

        if (!$product) {
            return $this->json('No product found for id' . $id, 404);
        }

        $data = [
            'id' => $product->getId(),
            'title' => $product->getTitle(),
            'price' => $product->getPrice(),
            'image' => $product->getImage()
        ];

        return $this->json($data);
    }
}
